#include <pcap.h>
#include <stdlib.h>
#include <sysexits.h>

static void callback(u_char *user, const struct pcap_pkthdr *h, const u_char *bytes)
{
}

int main(int _argc, char** _argv)
{
	char errbuf[PCAP_ERRBUF_SIZE] = { 0, };
	char filter_exp[] = "icmp";
	char dev[] = "eth-dongle";
	pcap_t* handle = NULL;
	struct bpf_program fp;
	int dispatched = 0;
	int err = EX_OK;

	handle = pcap_open_live(dev, PCAP_BUF_SIZE, PCAP_OPENFLAG_PROMISCUOUS, 1000, errbuf);
	if (!handle)
	{
		fprintf(stderr, "Couldn't open device: %s\n", errbuf);
		err = EX_OSERR;
		goto out;
	}

	if (pcap_datalink(handle) != DLT_EN10MB)
	{
		fprintf(stderr, "Device %s doesn't provide Ethernet headers - not supported\n", dev);
		err = EX_OSERR;
		goto close_out;
	}

	if (pcap_compile(handle, &fp, filter_exp, 0, PCAP_NETMASK_UNKNOWN) == -1)
	{
		fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
		err = EX_USAGE;
		goto close_out;
	}

	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
		err = EX_OSERR;
		goto close_out;
	}

    for (;;)
	{
		dispatched = pcap_dispatch(handle, 10, callback, NULL);

		if (dispatched == PCAP_ERROR)
			break;

		if (dispatched == PCAP_ERROR_BREAK)
			break;
	}

close_out:
	pcap_close(handle);

out:
	exit(err);
}
